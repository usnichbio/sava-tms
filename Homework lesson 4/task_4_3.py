# Решение через цикл for
dict_a = {'test': 'test_value', 'europe': 'eur', 'dollar': 'usd', 'ruble': 'rub'}
keys_dict_a = list(dict_a.keys())
values_dict_a = list(dict_a.values())
index = 0

for i in keys_dict_a:
    keys_dict_a[index] = i + str(len(i))
    index += 1

new_dict = dict(zip(keys_dict_a, values_dict_a))
print(new_dict)

#Решение через цикл while

dict_a = {'test': 'test_value', 'europe': 'eur', 'dollar': 'usd', 'ruble': 'rub'}
keys_dict_a = list(dict_a.keys())
values_dict_a = list(dict_a.values())
list_b = list(dict_a.keys())
i_1 = 0
i = 0

while i_1 < len(keys_dict_a):
      keys_dict_a[i_1] += str(len(keys_dict_a[i_1]))
      i_1 += 1

while True:
    if  list_b[i] in keys_dict_a[i]:
        dict_a[keys_dict_a[i]] = dict_a.pop(list_b[i])
    i += 1
    if i >= len(keys_dict_a):
        break
print(dict_a)

