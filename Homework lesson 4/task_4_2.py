# Решение через цикл for
list_a = [1, 5, 8, 99, 55, 45, 31, 4, 54, 98]
counter = 0
for i in list_a:
    if i % 2 == 0:
        counter += 1
print(counter)

# Решение через цикл while
list_a = [1, 5, 8, 99, 55, 45, 31, 4, 54, 98]
counter = 0
i = 0
while i < len(list_a):
    if list_a[i] % 2 == 0:
        counter += 1
    i += 1
print(counter)

