# Решение через цикл for
f_1 = 1
f_2 = 1
n = 15
print(f_1, end=' ')
print(f_2, end=' ')
for i in range(2, n):
    f_1, f_2 = f_2, f_1 + f_2
    print(f_2, end=' ')

# Решение через цикл while
print()

f_1 = 1
f_2 = 1
i = 0
while True:
    summa = f_1 + f_2
    f_1 = f_2
    f_2 = summa
    i = i + 1

print(f_2)
